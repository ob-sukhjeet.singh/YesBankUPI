//
//  ApiBase.swift
//  SDKTest
//
//  Created by Sukhjeet Singh on 21/03/22.
//

import Foundation
import CommonCrypto

final class ApiBase{
    
    
    enum EndPoint:String{
        static let host = "uatsky.yesbank.in"
        
        case checkDeviceID
        
        /// get url
        var url:URL{
            return URL(string: self.stringValue)!
            
        }
        /// get url String
        var stringValue:String{
            switch self {
            case .checkDeviceID:
                return getUrl(endpoint: EndPoints.checkDeviceID.rawValue)
            }
        }
        private func getUrl(endpoint:String) ->String {
            return "\(scheme.Https.rawValue)\(ApiBase.EndPoint.host)\(Path.path.rawValue)\(endpoint)"
        }
        /// Scheme
        enum scheme:String{
            case Http = "http://"
            case Https = "https://"
        }
        ///Path
        enum Path:String{
            case path = "/app"
        }
        
        enum EndPoints:String{
            case checkDeviceID = "/uat/IntegrationServices/deviceStatusMEService"
        }
    }
    
    class private func apiRequestFunction<RequestBody: Encodable, ResponseBody: Decodable>(url: URL, responseType: ResponseBody.Type, body: RequestBody?,httpMethod:String, completion: @escaping (ResponseBody?, ErrorModel?) -> Void) -> URLSessionDataTask {
        var request = URLRequest(url: url)
        request.httpMethod = httpMethod
        
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        request.setValue("tI8xM8gL0sA1sH0cX2eQ8hC1kS1xT4kH4uQ7sF6hO5xE0qG1cL", forHTTPHeaderField: "X-IBM-Client-Secret")
        request.setValue("275841c3-09e0-42bd-82bd-f9818a1416df", forHTTPHeaderField: "X-IBM-Client-ID")
        request.setValue(AppDelegate.tokenid, forHTTPHeaderField: "Authorization")
        request.setValue("YES0000000054393", forHTTPHeaderField: "Id")
        
        var requiredDataString = String()
        
        if body != nil{
            request.httpBody = body as? Data
        }
        let task = URLSession.shared.dataTask(with: request){ data, response, error in
            guard let data = data else {
                completion(nil,nil)
                return
            }
            
            if let dataString = String(data: data, encoding: .utf8) {
                requiredDataString = String(dataString.filter { !"\r\n\t".contains($0) })
            } else {
                print("not a valid UTF-8 sequence")
            }
            let requiredData = Data(requiredDataString.utf8)
            print(String(data: requiredData, encoding: .utf8))
            let decoder = JSONDecoder()
            
            do {
                let responseObject = try decoder.decode(ResponseBody.self, from: requiredData)
                DispatchQueue.main.async {
                    completion(responseObject, nil)
                }
            }catch{
                do{
                    let errorResponse = try decoder.decode(ErrorModel.self, from: data)
                    DispatchQueue.main.async {
                        completion(nil, errorResponse)
                    }
                } catch {
                    DispatchQueue.main.async {
                        completion(nil, nil)
                    }
                }
            }
        }
        task.resume()
        return task
    }
    
    class func encryptToAes(data:Data, keyData:Data, ivData:Data, operation:Int) -> String {
        let cryptLength = size_t(data.count + kCCBlockSizeAES128)
        var cryptData = Data(count:cryptLength)
        
        let keyLength = size_t(kCCKeySizeAES256)
        let options = CCOptions(kCCOptionPKCS7Padding)
        
        
        var numBytesEncrypted :size_t = 0
        
        let cryptStatus = cryptData.withUnsafeMutableBytes {cryptBytes in
            data.withUnsafeBytes {dataBytes in
                ivData.withUnsafeBytes {ivBytes in
                    keyData.withUnsafeBytes {keyBytes in
                        CCCrypt(CCOperation(operation),
                                CCAlgorithm(kCCAlgorithmAES),
                                options,
                                keyBytes, keyLength,
                                ivBytes,
                                dataBytes, data.count,
                                cryptBytes, cryptLength,
                                &numBytesEncrypted)
                    }
                }
            }
        }
        
        if UInt32(cryptStatus) == UInt32(kCCSuccess) {
            cryptData.removeSubrange(numBytesEncrypted..<cryptData.count)
            
        } else {
            print("Error: \(cryptStatus)")
        }
        
        return cryptData.base64EncodedString()
    }
    
    class func checkDeviceID() {
        let paramString = "YES0000000054393|GKvhzZ3H1nf|123|com.example.yes_sdk_project|19.112088,72.930825|Mumbai|192.168.1.178|MOB|7446489340284893413847123456709876|IOS 10.2|\(AppDelegate.tokenid)|\(AppDelegate.tokenid)|1|1|1|||||||||NA|NA".data(using: .utf8)!
        let key = "a678196b035a7a5846f29595ffb40f7d".data(using: .utf8)!
        let ivData = Data([UInt8](repeating: 0, count: 16))
        
        let requestMsg = ApiBase.encryptToAes(data: paramString, keyData: key, ivData: ivData, operation: kCCEncrypt)
        print("YES0000000054393|GKvhzZ3H1nf|123|ai.Onebanc.SimBind|28.45176,77.05055|Gurugram|192.168.1.178|MOB|7446489340284893413847123456709876|IOS 15.4|\(AppDelegate.tokenid)|\(AppDelegate.tokenid)|1|1|1|||||||||NA|NA")
        print(requestMsg)
        let params: [String: Any] = [
            "requestMsg": requestMsg,
            "PgMerchantId": "YES0000000054393",
            "appGenId": "\(UUID().uuidString)"
        ]
        
        let body = (try? JSONSerialization.data(withJSONObject: params))!
        //print(String(data: body, encoding: .utf8))
        apiRequestFunction(url: EndPoint.checkDeviceID.url, responseType: CheckDeviceIdModel.self, body: body, httpMethod: "POST") { result, error in
        
        }
    }
    
}
