//
//  ViewController.swift
//  SDKTest
//
//  Created by Sukhjeet Singh on 17/02/22.
//

import UIKit
import Foundation
import CommonLibrary
import CryptoKit
import CommonCrypto

class ViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        //YES0000000054393|GKvhzZ3H1nf|123|ai.OneBanc.SDK-POC|19.112088,72.930825|Mumbai|192.168.1.169|MOB|7446489340284893413847123456709876|IOS|9A3E660463431B22E9DA6AAD96E2392A73F672496E1B69126DBEFEA0C236B61FCEB09F4F95B41FD2BEB03F5D3F5AE014|9A3E660463431B22E9DA6AAD96E2392A73F672496E1B69126DBEFEA0C236B61FCEB09F4F95B41FD2BEB03F5D3F5AE014|3E90AED843614ACD8474A0EBF9538639|3E90AED843614ACD8474A0EBF9538639|3E90AED843614ACD8474A0EBF9538639|1|1|1||||||NA|NA
        
        getToken { result in
            print(result)
            initializeSDK { result in
                if result {
                    ApiBase.checkDeviceID()
                }
                sendSMS { result in
//                    pay { result in
//                        balanceEnquiry { result in
//                            setupMPIN { result in
//                                collectAuth { result in
//
//                                }
//                            }
//                        }
//                    }
                }
            }
        }
        
        
        
        
    }
    
    
}




func getToken(completion: @escaping (Bool) -> Void) {
    MGUPIController.getDeviceToken(withPspID: "YES0000000054393", encryptionKey: "a678196b035a7a5846f29595ffb40f7d", flag: "001") { tokenId in
        if tokenId != nil {
            AppDelegate.tokenid = tokenId!
            print(tokenId)
        } else {
            print("Token Nil")
        }
        completion(true)
    }
}

func initializeSDK(completion: @escaping (Bool) -> Void) {
    MGUPIController.initialize(withPspId: "YES0000000054393", encryptionKey: "a678196b035a7a5846f29595ffb40f7d", appType: "MOB", capabilities: "7446489340284893413847123456709876", geocode: "28.41863081280882,77.03811646195018", location: "Gurugram", systemWifiMac: "3c:a6:f6:40:74:d1", systemBluetoothMac: "1", systemUniqueId: "1", sysytemSimSerial: AppDelegate.tokenid) { result, error in
        if result {
            print(result)
        } else {
            print(error)
        }
        completion(true)
    }
}

func sendSMS(completion: @escaping (Bool) -> Void) {
    MGUPIController.sendSMS(withSmsContent: "YESUPIUAT", recipentMobNum: "9654178539", stringSmsKey: "ADEEC43E866BF59E", appGenId: "45045458343642FE9EB47D6577C2316B") { result, text, error in
        if result {
            print(result, text, error)
        } else {
            print(error, result, text)
        }
        completion(true)
    }
}

//TODO: Not Working
//MARK: pay
func pay(completion: @escaping (Bool) -> Void) {
    let object = PaymentInfo()
    object.accountId = ""
    object.accountNo = ""
    object.amount = "1.00"
    object.ifscCode = ""
    object.paymentType = "P2M"
    object.iin = ""
    object.pspId = "YES0000000054393"
    object.pspRefId = ""
    object.payeeVPA = "onebanc@yesb"
    object.payeeMMID = ""
    object.payerMobileNo = ""
    object.payeeAadhaarNo = ""
    object.remarks = ""
    object.payeeName = "Payal"
    object.add1 = ""
    object.add2 = ""
    object.add3 = ""
    object.add4 = ""
    object.add5 = ""
    object.add6 = ""
    object.add7 = ""
    object.add8 = ""
    object.add9 = "NA"
    object.add10 = "NA"
    MGUPIController.pay(withRequest: object) { result, ob, error in
        print("Pay Result")
        if result {
            print(ob)
        } else {
            print(error, ob, result)
        }
        completion(true)
        
    }
}
//MARK: balance enquiry
func balanceEnquiry(completion: @escaping (Bool) -> Void) {
    let object = RequestInfo()
    object.pspId = "YES0000000054393"
    object.pspRefId = "ea3401cec22dec24e9756a71904b8515"
    object.accountId = "1234"
    object.virtualAddress = "rohot@yesb"
    object.mobileNo = "9223390909"
    object.add1 =  ""
    object.add2 = ""
    object.add3 = ""
    object.add4 = ""
    object.add5 = ""
    object.add6 = ""
    object.add7 = ""
    object.add8 = ""
    object.add9 = "NA"
    object.add10 = "NA"
    MGUPIController.balanceEnquiry(object) { result, response, error in
        print("balanceEnquiry", result, response, error)
        completion(true)
        
    }
}
//MARK: setup MPIN
func setupMPIN(completion: @escaping (Bool) -> Void) {
    let object = SetMPINInfo()
    object.pspId = "UPI000000000001"
    object.pspRefId = "ea3401cec22dec24e9756a71904b8515"
    object.accountId = "1234"
    object.lastSixDigitsOfDebotCard = "123456"
    object.expDate = "1019"
    object.virtualAddress = "rohot@yesb"
    object.add1 = ""
    object.add2 = ""
    object.add3 = ""
    object.add4 = ""
    object.add5 = ""
    object.add6 = ""
    object.add7 = ""
    object.add8 = ""
    object.add9 = "NA"
    object.add10 = "NA"
    MGUPIController.setMPINWithRequest(object) { result, response, error in
        print("setupMPIN", result, response, error)
        completion(true)
        
    }
    
}
//MARK: Collect Authorize
func collectAuth(completion: @escaping (Bool) -> Void) {
    let object = CollectApproveInfo()
    object.pspId = "YES0000000054393"
    object.pspRefId = "a678196b035a7a5846f29595ffb40f"
    object.payerVPA = "rohit@yesb"
    object.accountId = "1234"
    object.npciRefNo = "4445545454"
    object.reqType = "R"
    object.add1 = ""
    object.add2 = ""
    object.add3 = ""
    object.add4 = ""
    object.add5 = ""
    object.add6 = ""
    object.add7 = ""
    object.add8 = ""
    object.add9 = "NA"
    object.add10 = "NA"
    MGUPIController.approveCollectRequest(withRequest: object) { result, response, error in
        print("collectAuth", result, response, error)
        completion(true)
    }
}
