//
//  MGUPIController.h
//  YesBankUPI
//  Created by Mindgate MAC on 24/04/2017.
//  Copyright © 2017 Mindgate. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "ResponseInfo.h"
#import "RequestInfo.h"

@interface MGUPIController : NSObject

+(void)initializeWithPspId:(NSString *)Id encryptionKey:(NSString *)encryptionKey appType:(NSString*)appTypeStr capabilities:(NSString*)capabilitiesStr geocode:(NSString*)geocodeStr location:(NSString*)locationStr systemWifiMac:(NSString*)wifimac systemBluetoothMac:(NSString*)bluetoothMac systemUniqueId:(NSString*)uniqueId sysytemSimSerial:(NSString*)simSerial withCallback:(void(^)(BOOL isSuccess , NSError *error))responseBlock ;

+(void)sendSMSWithSmsContent:(NSString *)smsContent recipentMobNum:(NSString *)strMobNum stringSmsKey:(NSString *)stringSmsKey appGenId:(NSString *)stringappGenId andCallback:(void (^)(BOOL isSuccess, NSString *responseString, NSError *error))responseBlock ;

+ (void)getDeviceTokenWithPspID:(NSString *)pspId encryptionKey:(NSString *)encryptionKey flag:(NSString *)flag callback:(void(^)(NSString *deviceToken))callbvack;

+(void)balanceEnquiry:(RequestInfo *)request completionHandler:(void (^)(BOOL isSuccess, BalanceEnquiryResponse *successObject, NSError * error))responseBlock;

+(void)changeMPINWithRequest:(RequestInfo *)request completionHandler:(void (^)(BOOL isSuccess, ChangeMPINResponse *successObject, NSError *error))responseBlock;

+(void)setMPINWithRequest:(SetMPINInfo*)request completionHandler:(void (^)(BOOL isSuccess, SetMpinResponse *successObject, NSError *error))responseBlock;

+(void)payWithRequest:(PaymentInfo*)request andCallback:(void(^)(BOOL isSuccess ,PaymentResponse *successObject, NSError *error))payCallback;

+(void)approveCollectRequestWithRequest:(CollectApproveInfo*)request andCallback:(void(^)(BOOL isSuccess ,CollectApproveResponse *successObject, NSError *error))approveCallback;

+(NSString *)encryptMessage:(NSString *)message withKey:(NSString *)key ;

+(NSString *)decryptMessage:(NSString *)message withKey:(NSString *)key ;

+(NSString *)getSDKVersion;

@end
