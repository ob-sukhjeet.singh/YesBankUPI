//
//  ResponseInfo.h
//  YesBankUPI
//
//  Created by Mindgate MAC on 24/04/2017.
//  Copyright © 2017 Mindgate. All rights reserved.

#import <Foundation/Foundation.h>

@interface ResponseInfo : NSObject

@property (strong,nonatomic) NSString *RefNo;
@property (strong,nonatomic) NSString *MeTxnId;
@property (strong,nonatomic) NSString *VirtualAddress;
@property (strong,nonatomic) NSString *Status;
@property (strong,nonatomic) NSString *StatusDesc;
@property (strong,nonatomic) NSString *RegDate;
@property (strong,nonatomic) NSString *RegRefId;
@property (strong,nonatomic) NSString *MobileNo;

@property (strong,nonatomic) NSString *Add1;
@property (strong,nonatomic) NSString *Add2;
@property (strong,nonatomic) NSString *Add3;
@property (strong,nonatomic) NSString *Add4;
@property (strong,nonatomic) NSString *Add5;
@property (strong,nonatomic) NSString *Add6;
@property (strong,nonatomic) NSString *Add7;
@property (strong,nonatomic) NSString *Add8;
@property (strong,nonatomic) NSString *Add9;
@property (strong,nonatomic) NSString *Add10;

@end

@interface CheckDeviceResponse : NSObject

@property (strong,nonatomic) NSString *pspRefNo;
@property (strong,nonatomic) NSString *StatusDesc;
@property (strong,nonatomic) NSString *DeviceStatus;
@property (strong,nonatomic) NSString *SmsStatus;
@property (strong,nonatomic) NSString *SimStatus;
@property (strong,nonatomic) NSString *MobileNo;
@property (strong,nonatomic) NSString *VirtualAddress;
@property (strong,nonatomic) NSString *GateWayNo;
@property (strong,nonatomic) NSString *smsContent;
@property (strong,nonatomic) NSString *Enckey;
@property (strong,nonatomic) NSString *UUID;
@property (strong,nonatomic) NSString *RegMsg;
@property (strong,nonatomic) NSString *SimState;
@property (strong,nonatomic) NSString *SimNo;
@property (strong,nonatomic) NSString *Add1;
@property (strong,nonatomic) NSString *Add2;
@property (strong,nonatomic) NSString *Add3;
@property (strong,nonatomic) NSString *Add4;
@property (strong,nonatomic) NSString *Add5;
@property (strong,nonatomic) NSString *Add6;
@property (strong,nonatomic) NSString *Add7;
@property (strong,nonatomic) NSString *Add8;
@property (strong,nonatomic) NSString *Add9;
@property (strong,nonatomic) NSString *Add10;

@end

@interface BankListResponse : NSObject

@property (strong,nonatomic) NSString *RefId;
@property (strong,nonatomic) NSString *BankDetails;
@property (strong,nonatomic) NSString *Status;
@property (strong,nonatomic) NSString *StatusDesc;
@property (strong,nonatomic) NSString *Add1;
@property (strong,nonatomic) NSString *Add2;
@property (strong,nonatomic) NSString *Add3;
@property (strong,nonatomic) NSString *Add4;
@property (strong,nonatomic) NSString *Add5;
@property (strong,nonatomic) NSString *Add6;
@property (strong,nonatomic) NSString *Add7;
@property (strong,nonatomic) NSString *Add8;
@property (strong,nonatomic) NSString *Add9;
@property (strong,nonatomic) NSString *Add10;

@end

@interface SecurityQuestionsResponse : NSObject

@property (strong,nonatomic) NSString *RefId;
@property (strong,nonatomic) NSString *SecQuesList;
@property (strong,nonatomic) NSString *Status;
@property (strong,nonatomic) NSString *StatusDesc;
@property (strong,nonatomic) NSString *Add1;
@property (strong,nonatomic) NSString *Add2;
@property (strong,nonatomic) NSString *Add3;
@property (strong,nonatomic) NSString *Add4;
@property (strong,nonatomic) NSString *Add5;
@property (strong,nonatomic) NSString *Add6;
@property (strong,nonatomic) NSString *Add7;
@property (strong,nonatomic) NSString *Add8;
@property (strong,nonatomic) NSString *Add9;
@property (strong,nonatomic) NSString *Add10;

@end

@interface FetchCustomerAccountsResponse : NSObject

@property (strong,nonatomic) NSString *RefId;
@property (strong,nonatomic) NSString *RegRequestId;
@property (strong,nonatomic) NSString *Status;
@property (strong,nonatomic) NSString *StatusDesc;
@property (strong,nonatomic) NSString *AccountList;
@property (strong,nonatomic) NSString *AccountHolderName;
@property (strong,nonatomic) NSString *Add1;
@property (strong,nonatomic) NSString *Add2;
@property (strong,nonatomic) NSString *Add3;
@property (strong,nonatomic) NSString *Add4;
@property (strong,nonatomic) NSString *Add5;
@property (strong,nonatomic) NSString *Add6;
@property (strong,nonatomic) NSString *Add7;
@property (strong,nonatomic) NSString *Add8;
@property (strong,nonatomic) NSString *Add9;
@property (strong,nonatomic) NSString *Add10;

@end

@interface RegisterAccountResponse : NSObject

@property (strong,nonatomic) NSString *RefId;
@property (strong,nonatomic) NSString *VirtualAddress;
@property (strong,nonatomic) NSString *Status;
@property (strong,nonatomic) NSString *StatusDesc;
@property (strong,nonatomic) NSString *RegRefId;
@property (strong,nonatomic) NSString *RegDate;
@property (strong,nonatomic) NSString *MobileNo;
@property (strong,nonatomic) NSString *Add1;
@property (strong,nonatomic) NSString *Add2;
@property (strong,nonatomic) NSString *Add3;
@property (strong,nonatomic) NSString *Add4;
@property (strong,nonatomic) NSString *Add5;
@property (strong,nonatomic) NSString *Add6;
@property (strong,nonatomic) NSString *Add7;
@property (strong,nonatomic) NSString *Add8;
@property (strong,nonatomic) NSString *Add9;
@property (strong,nonatomic) NSString *Add10;

@end

@interface FetchCustomerProfileResponse : NSObject

@property (strong,nonatomic) NSString *RefId;
@property (strong,nonatomic) NSString *VirtualAddress;
@property (strong,nonatomic) NSString *Status;
@property (strong,nonatomic) NSString *StatusDesc;
@property (strong,nonatomic) NSString *AccountList;
@property (strong,nonatomic) NSString *CustomerName;
@property (strong,nonatomic) NSString *MobileNo;
@property (strong,nonatomic) NSString *Add1;
@property (strong,nonatomic) NSString *Add2;
@property (strong,nonatomic) NSString *Add3;
@property (strong,nonatomic) NSString *Add4;
@property (strong,nonatomic) NSString *Add5;
@property (strong,nonatomic) NSString *Add6;
@property (strong,nonatomic) NSString *Add7;
@property (strong,nonatomic) NSString *Add8;
@property (strong,nonatomic) NSString *Add9;
@property (strong,nonatomic) NSString *Add10;

@end

@interface PaymentResponse : NSObject

@property (strong,nonatomic) NSString *RefNo;
@property (strong,nonatomic) NSString *TxnId;
@property (strong,nonatomic) NSString *Amount;
@property (strong,nonatomic) NSString *TransAuthDate;
@property (strong,nonatomic) NSString *Status;
@property (strong,nonatomic) NSString *StatusDesc;
@property (strong,nonatomic) NSString *ResponseCode;
@property (strong,nonatomic) NSString *ApprovalNo;
@property (strong,nonatomic) NSString *PayerVA;
@property (strong,nonatomic) NSString *NPCITxnId;
@property (strong,nonatomic) NSString *CustRefId;
@property (strong,nonatomic) NSString *Add1;
@property (strong,nonatomic) NSString *Add2;
@property (strong,nonatomic) NSString *Add3;
@property (strong,nonatomic) NSString *Add4;
@property (strong,nonatomic) NSString *Add5;
@property (strong,nonatomic) NSString *Add6;
@property (strong,nonatomic) NSString *Add7;
@property (strong,nonatomic) NSString *Add8;
@property (strong,nonatomic) NSString *Add9;
@property (strong,nonatomic) NSString *Add10;

@property (strong,nonatomic) NSString *tempRefID ;  // Payal added

@end


@interface CollectRequestListResponse : NSObject

@property (strong,nonatomic) NSString *RefNo;
@property (strong,nonatomic) NSString *Status;
@property (strong,nonatomic) NSString *StatusDesc;
@property (strong,nonatomic) NSString *CollReqList;
@property (strong,nonatomic) NSString *VirtualAddress;
@property (strong,nonatomic) NSString *Add1;
@property (strong,nonatomic) NSString *Add2;
@property (strong,nonatomic) NSString *Add3;
@property (strong,nonatomic) NSString *Add4;
@property (strong,nonatomic) NSString *Add5;
@property (strong,nonatomic) NSString *Add6;
@property (strong,nonatomic) NSString *Add7;
@property (strong,nonatomic) NSString *Add8;
@property (strong,nonatomic) NSString *Add9;
@property (strong,nonatomic) NSString *Add10;

@end

@interface CollectApproveResponse : NSObject

@property (strong,nonatomic) NSString *RefNo;
@property (strong,nonatomic) NSString *TxnId;
@property (strong,nonatomic) NSString *NPCIRefNo;
@property (strong,nonatomic) NSString *Status;
@property (strong,nonatomic) NSString *StatusDesc;
@property (strong,nonatomic) NSString *PayeeName;
@property (strong,nonatomic) NSString *Remarks; // not there
@property (strong,nonatomic) NSString *Amount;
@property (strong,nonatomic) NSString *AuthDate;
@property (strong,nonatomic) NSString *ResponseCode;
@property (strong,nonatomic) NSString *ApprovalNo;
@property (strong,nonatomic) NSString *CustRefId;
@property (strong,nonatomic) NSString *MobileNo; // not there
@property (strong,nonatomic) NSString *AccountNo; // not there
@property (strong,nonatomic) NSString *BankName; // not there
@property (strong,nonatomic) NSString *PayerVPA;
@property (strong,nonatomic) NSString *PayeeVPA; // not there & Add TempRefID
@property (strong,nonatomic) NSString *Add1;
@property (strong,nonatomic) NSString *Add2;
@property (strong,nonatomic) NSString *Add3;
@property (strong,nonatomic) NSString *Add4;
@property (strong,nonatomic) NSString *Add5;
@property (strong,nonatomic) NSString *Add6;
@property (strong,nonatomic) NSString *Add7;
@property (strong,nonatomic) NSString *Add8;
@property (strong,nonatomic) NSString *Add9;
@property (strong,nonatomic) NSString *Add10;

@end

@interface CollectInitiateResponse : NSObject

@property (strong,nonatomic) NSString *TxnId;
@property (strong,nonatomic) NSString *RefNo;
@property (strong,nonatomic) NSString *Amount;
@property (strong,nonatomic) NSString *Status;
@property (strong,nonatomic) NSString *StatusDesc;
@property (strong,nonatomic) NSString *VirtualAddress;
@property (strong,nonatomic) NSString *CustRefId;

@property (strong,nonatomic) NSString *Add1;
@property (strong,nonatomic) NSString *Add2;
@property (strong,nonatomic) NSString *Add3;
@property (strong,nonatomic) NSString *Add4;
@property (strong,nonatomic) NSString *Add5;
@property (strong,nonatomic) NSString *Add6;
@property (strong,nonatomic) NSString *Add7;
@property (strong,nonatomic) NSString *Add8;
@property (strong,nonatomic) NSString *Add9;
@property (strong,nonatomic) NSString *Add10;

@end


@interface ChangeMPINResponse : NSObject

@property (strong,nonatomic) NSString *RefNo;
@property (strong,nonatomic) NSString *YBRefNo;
@property (strong,nonatomic) NSString *TxnId;
@property (strong,nonatomic) NSString *Status;
@property (strong,nonatomic) NSString *StatusDesc;
@property (strong,nonatomic) NSString *Add1;
@property (strong,nonatomic) NSString *Add2;
@property (strong,nonatomic) NSString *Add3;
@property (strong,nonatomic) NSString *Add4;
@property (strong,nonatomic) NSString *Add5;
@property (strong,nonatomic) NSString *Add6;
@property (strong,nonatomic) NSString *Add7;
@property (strong,nonatomic) NSString *Add8;
@property (strong,nonatomic) NSString *Add9;
@property (strong,nonatomic) NSString *Add10;

@end


@interface SetMpinResponse : NSObject

@property (strong,nonatomic) NSString *RefNo;
@property (strong,nonatomic) NSString *YBRefNo;
@property (strong,nonatomic) NSString *TxnId;
@property (strong,nonatomic) NSString *Status;
@property (strong,nonatomic) NSString *StatusDesc;
@property (strong,nonatomic) NSString *Add1;
@property (strong,nonatomic) NSString *Add2;
@property (strong,nonatomic) NSString *Add3;
@property (strong,nonatomic) NSString *Add4;
@property (strong,nonatomic) NSString *Add5;
@property (strong,nonatomic) NSString *Add6;
@property (strong,nonatomic) NSString *Add7;
@property (strong,nonatomic) NSString *Add8;
@property (strong,nonatomic) NSString *Add9;
@property (strong,nonatomic) NSString *Add10;

@end


@interface BalanceEnquiryResponse : NSObject

@property (strong,nonatomic) NSString *RefNo;
@property (strong,nonatomic) NSString *Status;
@property (strong,nonatomic) NSString *StatusDesc;
@property (strong,nonatomic) NSString *Add1;
@property (strong,nonatomic) NSString *Add2;
@property (strong,nonatomic) NSString *Add3;
@property (strong,nonatomic) NSString *Add4;
@property (strong,nonatomic) NSString *Add5;
@property (strong,nonatomic) NSString *Add6;
@property (strong,nonatomic) NSString *Add7;
@property (strong,nonatomic) NSString *Add8;
@property (strong,nonatomic) NSString *Add9;
@property (strong,nonatomic) NSString *Add10;
@property (strong,nonatomic) NSString *AccountName;
@property (strong,nonatomic) NSString *AccountBalance;

@end

@interface GetSecretQuestionResponseInfo : NSObject

@property (strong,nonatomic) NSString *YblRefNo;
@property (strong,nonatomic) NSString *MerchantTrnxID;
@property (strong,nonatomic) NSString *Status;
@property (strong,nonatomic) NSString *StatusDesc;
@property (strong,nonatomic) NSString *SecretQuestionList;

@property (strong,nonatomic) NSString *Add1;
@property (strong,nonatomic) NSString *Add2;
@property (strong,nonatomic) NSString *Add3;
@property (strong,nonatomic) NSString *Add4;
@property (strong,nonatomic) NSString *Add5;
@property (strong,nonatomic) NSString *Add6;
@property (strong,nonatomic) NSString *Add7;
@property (strong,nonatomic) NSString *Add8;
@property (strong,nonatomic) NSString *Add9;
@property (strong,nonatomic) NSString *Add10;



@end



