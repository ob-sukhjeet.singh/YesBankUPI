//
//  RequestInfo.h
//  YesBankUPI
//
//  Created by Mindgate MAC on 24/04/2017.
//  Copyright © 2017 Mindgate. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface RequestInfo : NSObject

@property(strong,nonatomic) NSString *pspId;
@property(strong,nonatomic) NSString *pspRefId;
@property(strong,nonatomic) NSString *accountId;
@property(strong,nonatomic) NSString *virtualAddress;
@property(strong,nonatomic) NSString *mobileNo;
@property(strong,nonatomic) NSString *Add1;
@property(strong,nonatomic) NSString *Add2;
@property(strong,nonatomic) NSString *Add3;
@property(strong,nonatomic) NSString *Add4;
@property(strong,nonatomic) NSString *Add5;
@property(strong,nonatomic) NSString *Add6;
@property(strong,nonatomic) NSString *Add7;
@property(strong,nonatomic) NSString *Add8;
@property(strong,nonatomic) NSString *Add9;
@property(strong,nonatomic) NSString *Add10;

@end


@interface CheckDeviceInfo : NSObject

@property(strong,nonatomic) NSString *pspId;
@property(strong,nonatomic) NSString *pspRefId;
@property(strong,nonatomic) NSString *regId;
@property(strong,nonatomic) NSString *walletId;
@property(strong,nonatomic) NSString *Add1;
@property(strong,nonatomic) NSString *Add2;
@property(strong,nonatomic) NSString *Add3;
@property(strong,nonatomic) NSString *Add4;
@property(strong,nonatomic) NSString *Add5;
@property(strong,nonatomic) NSString *Add6;
@property(strong,nonatomic) NSString *Add7;
@property(strong,nonatomic) NSString *Add8;
@property(strong,nonatomic) NSString *Add9;
@property(strong,nonatomic) NSString *Add10;

@end

@interface FetchCustomerAccountsInfo : NSObject

@property(strong,nonatomic) NSString *pspId;
@property(strong,nonatomic) NSString *pspRefId;
@property(strong,nonatomic) NSString *virtualAddress;
@property(strong,nonatomic) NSString *bankCode;
@property(strong,nonatomic) NSString *Add1;
@property(strong,nonatomic) NSString *Add2;
@property(strong,nonatomic) NSString *Add3;
@property(strong,nonatomic) NSString *Add4;
@property(strong,nonatomic) NSString *Add5;
@property(strong,nonatomic) NSString *Add6;
@property(strong,nonatomic) NSString *Add7;
@property(strong,nonatomic) NSString *Add8;
@property(strong,nonatomic) NSString *Add9;
@property(strong,nonatomic) NSString *Add10;

@end

@interface RegisterAccountInfo : NSObject

@property(strong,nonatomic) NSString *pspId;
@property(strong,nonatomic) NSString *pspRefId;
@property(strong,nonatomic) NSString *RegRequestId;
@property(strong,nonatomic) NSString *virtualAddress;
@property(strong,nonatomic) NSString *AccountId;
@property(strong,nonatomic) NSString *CustomerName;
@property(strong,nonatomic) NSString *SecQuestionId;
@property(strong,nonatomic) NSString *SecAnswer;
@property(strong,nonatomic) NSString *RegId;
@property(strong,nonatomic) NSString *WalletId;

@property(strong,nonatomic) NSString *Add1;
@property(strong,nonatomic) NSString *Add2;
@property(strong,nonatomic) NSString *Add3;
@property(strong,nonatomic) NSString *Add4;
@property(strong,nonatomic) NSString *Add5;
@property(strong,nonatomic) NSString *Add6;
@property(strong,nonatomic) NSString *Add7;
@property(strong,nonatomic) NSString *Add8;
@property(strong,nonatomic) NSString *Add9;
@property(strong,nonatomic) NSString *Add10;

@end

@interface FetchCustomerProfileInfo : NSObject

@property(strong,nonatomic) NSString *pspId;
@property(strong,nonatomic) NSString *pspRefId;
@property(strong,nonatomic) NSString *Add1;
@property(strong,nonatomic) NSString *Add2;
@property(strong,nonatomic) NSString *Add3;
@property(strong,nonatomic) NSString *Add4;
@property(strong,nonatomic) NSString *Add5;
@property(strong,nonatomic) NSString *Add6;
@property(strong,nonatomic) NSString *Add7;
@property(strong,nonatomic) NSString *Add8;
@property(strong,nonatomic) NSString *Add9;
@property(strong,nonatomic) NSString *Add10;

@end


@interface SetMPINInfo : NSObject

@property(strong,nonatomic) NSString *pspId;
@property(strong,nonatomic) NSString *pspRefId;
@property(strong,nonatomic) NSString *accountId;
@property(strong,nonatomic) NSString *lastSixDigitsOfDebotCard;
@property(strong,nonatomic) NSString *expDate;
@property(strong,nonatomic) NSString *virtualAddress;
@property(strong,nonatomic) NSString *Add1;
@property(strong,nonatomic) NSString *Add2;
@property(strong,nonatomic) NSString *Add3;
@property(strong,nonatomic) NSString *Add4;
@property(strong,nonatomic) NSString *Add5;
@property(strong,nonatomic) NSString *Add6;
@property(strong,nonatomic) NSString *Add7;
@property(strong,nonatomic) NSString *Add8;
@property(strong,nonatomic) NSString *Add9;
@property(strong,nonatomic) NSString *Add10;

@end

@interface CollectInitiateInfo : NSObject

@property(strong,nonatomic) NSString *pspId;
@property(strong,nonatomic) NSString *pspRefId;
@property(strong,nonatomic) NSString *payerVPA;
@property(strong,nonatomic) NSString *amount;
@property(strong,nonatomic) NSString *remarks;
@property(strong,nonatomic) NSString *expTime;
@property(strong,nonatomic) NSString *Add1;
@property(strong,nonatomic) NSString *Add2;
@property(strong,nonatomic) NSString *Add3;
@property(strong,nonatomic) NSString *Add4;
@property(strong,nonatomic) NSString *Add5;
@property(strong,nonatomic) NSString *Add6;
@property(strong,nonatomic) NSString *Add7;
@property(strong,nonatomic) NSString *Add8;
@property(strong,nonatomic) NSString *Add9;
@property(strong,nonatomic) NSString *Add10;

@end

@interface CollectApproveInfo : NSObject

@property(strong,nonatomic) NSString *PspId;
@property(strong,nonatomic) NSString *PspRefId;
@property(strong,nonatomic) NSString *PayerVPA;
@property(strong,nonatomic) NSString *AccountId;
@property(strong,nonatomic) NSString *NPCIRefNo;
@property(strong,nonatomic) NSString *ReqType;
@property(strong,nonatomic) NSString *Add1;
@property(strong,nonatomic) NSString *Add2;
@property(strong,nonatomic) NSString *Add3;
@property(strong,nonatomic) NSString *Add4;
@property(strong,nonatomic) NSString *Add5;
@property(strong,nonatomic) NSString *Add6;
@property(strong,nonatomic) NSString *Add7;
@property(strong,nonatomic) NSString *Add8;
@property(strong,nonatomic) NSString *Add9;
@property(strong,nonatomic) NSString *Add10;

@end

@interface PaymentInfo : NSObject

@property(strong,nonatomic) NSString *pspId;
@property(strong,nonatomic) NSString *pspRefId;
@property(strong,nonatomic) NSString *paymentType;
@property(strong,nonatomic) NSString *payeeVPA;
@property(strong,nonatomic) NSString *payeeMMID;
@property(strong,nonatomic) NSString *payerMobileNo;
@property(strong,nonatomic) NSString *payeeAadhaarNo;
@property(strong,nonatomic) NSString *iin;
@property(strong,nonatomic) NSString *accountNo;
@property(strong,nonatomic) NSString *ifscCode;
@property(strong,nonatomic) NSString *accountId;
@property(strong,nonatomic) NSString *amount;
@property(strong,nonatomic) NSString *remarks;
@property(strong,nonatomic) NSString *payeeName;
@property(strong,nonatomic) NSString *Add1;
@property(strong,nonatomic) NSString *Add2;
@property(strong,nonatomic) NSString *Add3;
@property(strong,nonatomic) NSString *Add4;
@property(strong,nonatomic) NSString *Add5;
@property(strong,nonatomic) NSString *Add6;
@property(strong,nonatomic) NSString *Add7;
@property(strong,nonatomic) NSString *Add8;
@property(strong,nonatomic) NSString *Add9;
@property(strong,nonatomic) NSString *Add10;

@end


@interface GetSecretQuestionRequestInfo : NSObject

@property (strong,nonatomic) NSString *MerchantTrnxID;
@property (strong,nonatomic) NSString *VirtualAddress;
@property (strong,nonatomic) NSString *Add1;
@property (strong,nonatomic) NSString *Add2;
@property (strong,nonatomic) NSString *Add3;
@property (strong,nonatomic) NSString *Add4;
@property (strong,nonatomic) NSString *Add5;
@property (strong,nonatomic) NSString *Add6;
@property (strong,nonatomic) NSString *Add7;
@property (strong,nonatomic) NSString *Add8;
@property (strong,nonatomic) NSString *Add9;
@property (strong,nonatomic) NSString *Add10;


@end

@interface CheckVPARequestInfo : NSObject

@property(strong,nonatomic) NSString *pspId;
@property(strong,nonatomic) NSString *pspRefId;
@property(strong,nonatomic) NSString *ReqType;
@property(strong,nonatomic) NSString *virtualAddress;
@property(strong,nonatomic) NSString *Add1;
@property(strong,nonatomic) NSString *Add2;
@property(strong,nonatomic) NSString *Add3;
@property(strong,nonatomic) NSString *Add4;
@property(strong,nonatomic) NSString *Add5;
@property(strong,nonatomic) NSString *Add6;
@property(strong,nonatomic) NSString *Add7;
@property(strong,nonatomic) NSString *Add8;
@property(strong,nonatomic) NSString *Add9;
@property(strong,nonatomic) NSString *Add10;

@end


